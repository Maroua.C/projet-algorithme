#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 18:17:38 2020

@author: marwa

"""
#on a besoin de la libairie heapq qui va nous servir a gerer notre abre binaire 
import heapq as hq

#importation de tkinter ici pour le step by step cad generer les onglets (les pop up) meme principe que avec BWT
from tkinter import *
from tkinter import ttk



class Huffman:
        def __init__(self,seq):
            """Constructeur de la class Huffman """
            self.seq=seq
            self.one_liste=[]
            
            #on les met en variables global de classe  car on va les utiliser plusieurs fois dans le code 
        
        def table_frequences(self,seq):
            """Retourne la frequences des nucleotides sous formes de dictionnaire """
            #J'ai mi un parametre a la fonction pour pouvoir faire les vérification de fréquence nucléotide
            
            frequence_nu = {}
            
            #boucle qui prend nu par nu de la sequence et quand nu deja present dans dico rajoute +1 sinon ca veut dire que c'est la premiere fois qu'on le croise 
            for nu in seq:
                if nu in frequence_nu:
                    frequence_nu[nu] = frequence_nu[nu] + 1
                else:
                    frequence_nu[nu] = 1
 
            return frequence_nu

        def make_abre_huffman(self,dico_freq):
            """ fonction qui implemente la liste arbre en se basant sur le dictionnaire de frequence """
    
            arbre = []
            for nu, poids in dico_freq.items():
                arbre.append([poids, nu])
                
            #on va obtenir une liste de liste 
            return arbre
    
    
        def arbre_huffman(self,seq,oui):
            """Fonction qui permet de generer l'arbre de Huffman  """
            #recupere le dicotionnaire de frequence
            dico_freq=self.table_frequences(self.seq)            
            print(dico_freq)
            
            #si oui est egale a 1 alors il faut faire l'affichage etape par etape d'ou la creation du pop up
            if oui ==1:
                #on crée une fenetre flottante (un pop up)
                fInfos = Toplevel()		  
                fInfos.title('Huffman')
                    
                fInfos.geometry("350x250") 
                #bouton pour quitter le pop up
                B_quit=Button(fInfos, text='Quitter', command=fInfos.destroy)
                B_quit.grid(column=3) 
    
            #liste temporaire utilise lors de l'inplémentation de l'arbre va nous permettre de sauvegarder les noeuds de intermdiaire
            transition_noeud=[]
    
            arbre =self.make_abre_huffman(dico_freq)
    
            #heapify permet de manipuler l'arbre en heap donc en utilisant la librairie
            hq.heapify(arbre)
            #affiche l'arbre de depart 
            print(arbre)
            #huff_abrbre pour l'implémenttion de l'arbre 
            huff_abrbre=[]
            while len(arbre) >= 2:
                    #on va pop le plus petit element de l'abre et le stocker dans une liste de 2 element 
                    #avec comme premiere element le poids et comme deuxieme element le nu
                    noeud1 = hq.heappop(arbre) 
                    noeud2 = hq.heappop(arbre) 
            
                    #on va cree une liste dans laquelle on somme les poids des noeuds et concatene les nu correspondand
                    transition_noeud=[noeud1[0] + noeud2[0],noeud1[1]+noeud2[1]]
                    #one_liste permet de concatener les transition_noeud pour pouvoir visualiser l'arbre  
                    self.one_liste.append(transition_noeud)
                    print(self.one_liste)
                    
                    #pour le step by step pour le tkinter
                    if oui == 1:                        
                        L=Label(fInfos,text=self.one_liste,fg="#075C69",font=("Georgia", 15))
                        L.grid(column=1)
                                    
                    #on va push le new noeud qu'on a crée dans l'abre 
                    hq.heappush(arbre, transition_noeud)
                    #concatene les noeuds les plus petits avec 0 et 1 pour avoir la sequence binaire, 
                    #on rajoute les nu aussi pour savoir le 0 ou 1 a quelle nu il correspond 
                    huff_abrbre.append('1'+ noeud1[1])
                    huff_abrbre.append('0'+ noeud2[1])

            
            print(huff_abrbre)
            return huff_abrbre

        def make_dico(self,list):
            """Permet d'obtenir un dictionnaire de l'arbre faire via la fonction arbre_huffman 
                Exemple :Si on a ['1G', '0A', '1C', '0GA', '1N', '0T', '1CGA', '0NT']
                alors on aura A = 100,G=101,N=01 , C=11 et T=00 il faut lire de droite a gauche """
            j=len(list)-1
            #print(j)
    
            #initialisaiton de variables 
            binaire=""
            dico_fill_nu={}
            #Dans A,T,C,G,N on va concatener le binaire en fonction du nucleotide (sera mit dans la clé du dico)
            A=C=T=G=N=""
   
            while j >= 0:
                #print(list[j])
                if '0' in list[j]:
                    #print("Zero")
                    binaire='0'
                elif '1' in list[j]:
                    #print("Un")
                    binaire='1'
        
                if 'A' in list[j]:
                    A = A+ binaire
         
                if 'T' in list[j]:
                    T = T+ binaire
        
                if 'C' in list[j]:
                    C = C+ binaire
        
                if 'G' in list[j]:
                    G =G+ binaire
        
                if 'N' in list[j]:
                    N =N+ binaire
            
                #print pour verifier que c'est dans le bon sens qu'on ajoute les nombres binaires 
                #print("AA",one_zero * (len(list[j])))
        
                j=j-1
                #implement j dans le sens decroissant car on lit de droite a gauche la liste 
                       
            #implemente le dictionnaire 
            dico_fill_nu={A:'A',T:'T',G:'G',C:'C',N:'N'}
            return dico_fill_nu

    

        def encodage(self,sequence,dico_crypt):
            """ on va crypter la sequence  en utilisant la sequence ADN et le dictionnaire generer a la fin on aura une sequence binaire"""
            #construit le dictionnaire inverse cad la cle sera le nucleotide et la valeur sera le code binaire correspondant
            #cette ligne(160) a été inspirer par un code vu sur internet 
            dico_inv = dict((dico_crypt[bits],bits) for bits in dico_crypt)
    
            #sequence_binaire contiendrai la sequence crypter cad ecrit en binaire 
            sequence_binaire = ''
    
            #boucle element par element sur la sequence  
            for element in sequence:
                #on va concatener dans sequence_binaire a chaque iteration et quand on trouve un nu via le dico_inv alors on met sa valeurs (la valeurs du dico)
                sequence_binaire = sequence_binaire + dico_inv[element]
                        
            return sequence_binaire


        def decodage(self,sequence_binaire,dico_crypt):
            """On va passer du binaire aux caracteres ACTG grace a la sequence binaire et le dictionnaire en input on aura la sequence ADN en output"""
            sequence = ''
            #codon va nous permettre de faire le lien entre la sequence crypter et le dictionnaire  
            codon = ''
    
            for bit in sequence_binaire:
                #codon on concatene bit par bit cad a chaque iteration on rajoute 1 ou 0 en fonction de ce que contient la sequence_binaire
                codon = codon+bit
                #a chaque fois on va tester si codon est contenu dans le dictionnaire
                if codon in dico_crypt.keys():
                    #si on retrouve le codon (qui est contenu dans la cle du dico) alors il suffit de prendre la valeurs de la cle du dico
                    #on obtiendra le nu correspondant car chaque clé est unique et correspond a un et un seul type de nu
                    sequence = sequence + dico_crypt[codon]
                    #on reinitialise le codon pour pouvoir continuer 
                    codon = ''
            
            return sequence
        
        def list_to_str(self,list):
            """ Convertie une liste en string"""
            str=""
            for i in range(len(list)):
                str = str + list[i]
        
            return str 
            
        def one_char(self,sequence_binaire):
            """Permet d'afficher sur un seul caracterer 8 bit de la sequence binaire -> pour un gain de place """
            #pour cliver la sequence binaire en 8 bits -> octet
            octet=""
            
            #pour implementer la new sequence compresser
            seq_oct=""

            compteur_bit=0
            
            for bit in sequence_binaire:
                #si la longeur de la variable octet est inférieur a 8 alors on incremente cette variable                 
                if len(octet) != 8:  
                    octet=octet+bit
                    #compteur_bit permet de savoir cb de bit on a utiliser pour la conversion 
                    compteur_bit+=1
                else:  
                    #affiche octet pour voir cb de sequence de 8bits on a fait 
                    print(octet)
                    seq_oct=seq_oct+chr(int(octet,2))
                    #reinitialiser la variable
                    octet=""
                    #ATTENTION !! il faut increment octet ici car sinon on ne prend pas en compte en bit ( a chaque tour de boucle ou l'ont va fans le if)
                    octet=octet+bit
                    
            #le dernier donc lui il se peut qu'il n'ai pas 8bit 
            print("Le dernier",octet)
            #compteur_bit nous indiquer tout les bits utiliser de sequence binaire donc on peut savoir ceux qui n'ont pas été utiliser
            print(compteur_bit)

            temp=octet
            #le met en variable global pour pouvoir l'utiliser quand on va repasser en binaire dans la fonction retour_binaire
            self.last_octer=octet
            print("AVANT l'ajout de 0",temp)
            
            #compteur de 0 qu'on rajoute
            compte_zero_add=0
            
            while len(temp) != 8:
                temp += '0'
                compte_zero_add+=1
            print("APRES l'ajout de 0",temp)
            print("Compteur de rajout",compte_zero_add)
            #affiche la longeur pour vérfier qu'on a bien une longeur égale a 8
            print(len(temp))
            #rajoute le dernier caractère une fois qu'on l'a convertie 
            seq_oct = seq_oct + chr(int(temp,2))
                      
            #REMARQUE: parfois on a l'impression qu'il manque un caractere mais c'est juste le caratere espace ' '
            print("liste de caractère",list(seq_oct))
            return seq_oct
        
        def retour_binaire(self,char):
           """ Passage du one charactere au binaire"""
           binaire=''
           i=0
           while i in range(0,len(char)-1): 
               #affichage du caractere -> vérification
               print("one char",char[i])
               #on va convertir le caractere en binaire et le concatener a la variable binaire 
               binaire =binaire+ format(ord(char[i]),'08b') 
               #print(format(ord(char[i]),'08b') )
               i=i+1
           binaire = binaire + self.last_octer           
           return(binaire)
           
        
        


#seq="NNTNACTTNGNNGTTNCCTATACCNNTACCAGGTTNNCATGTTTAATGCANCC"
##seq="NNTNACTTNGNNGTTNCCTATACCT"
##seq="NNTNACTTNGNATGCTNGTGCACTATACCT"
#Huff=Huffman(seq)
#
#
#     
#elements=Huff.arbre_huffman(seq,0)
##print(table_frequences(seq))
#dico_crypt=Huff.make_dico(elements)
#print(dico_crypt)  
#  
#print('\n')
#binaire_text=Huff.encodage(seq,dico_crypt)
#print("encodage",binaire_text)
#print('\n')
#str_text=Huff.decodage(binaire_text,dico_crypt)
#print("decodage",str_text)
#print('\n')
#
#print('Verification :')
#print("TABLE DE FREQ DE DECODE:")
#print(Huff.table_frequences(str_text))
#  
##print("EST CE LA MEME CHOSE QUE:")
##print(Huff.table_frequences(seq))     
##print('\n') 
#
#onechar=Huff.one_char(binaire_text)
#   
#print("un caractère",onechar)
#print('\n') 
#back_to_bin=Huff.retour_binaire(onechar)                 
#print("revient au binaire",back_to_bin)
#print('\n')  
#print('Verification :')
#print(len(back_to_bin),"apres")
#print(len(binaire_text),"avant")
#print(binaire_text)
#str_text=Huff.decodage(back_to_bin,dico_crypt)
#print("decodage",str_text)
#print('\n')





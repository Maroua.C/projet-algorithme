#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 10:15:16 2020

@author: marwa

"""
from tkinter import *
from tkinter import ttk
from tkinter import filedialog

import HuffmanV3
 
class Huffman_graph:
    def __init__(self):
        #stocke le chemin du fichier
        self.filename=""
        #stocke la sequence du fichier
        self.seq=""
        
        #stock l'arbre sous forme de liste
        self.arbre=[]
        
        #stocke la sequence binaire 
        self.seq_bin=""

        #on les met en variables global de classe  car on va les utiliser plusieurs fois dans le code 
        
    def Huffman_get_path_fichier(self,frame):
        """ fonction qui permet d'obtenir le chemin du fichier """
        
        self.filename =filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("Text Files", "*.txt"),("all files","*.*")))
        print(self.filename)
                      
        f=open(self.filename,'r')
        f=f.read()
        
        #Commande pour enlever les \n
        self.seq=f.rstrip()  
        
        self.Import_classHUFF(frame)
        
    def Import_classHUFF(self,frame):
        """Cette fonction permet juste de faire qql label et d'appeler la class Huffman du fichier HuffmanV3 c'est plus pratique de l'appeler dans une fonction apart  """
        #importe la class Huffman du fichier HuffmanV3
        self.class_H=HuffmanV3.Huffman(self.seq)
        
        L_seq=Label(frame,text="La sequence input:",fg="#075C69", font=("Georgia", 12))
        L_seq.grid(row=3, column=1, sticky=W)
        
        L_seq2=Label(frame,text=self.seq,fg="#578F97", font=("Georgia", 12))
        L_seq2.grid(row=3, column=4)


    def Frequence(self,frame,row):
        """ fonction permettant d'appeler la fonction generant le dictionnaire de frequence de  la class Huffman du fichier HuffmanV3"""
        #on appel la fonction en question
        dicofreq=self.class_H.table_frequences(self.seq)
        
        L_frequence=Label(frame,text=dicofreq,fg="#578F97", font=("Georgia", 12))
        L_frequence.grid(row=row, column=4, sticky=W)
        
        
    def Arbre(self,frame,row,choix):
        """ fonction permettant d'appeler la fonction generant l'arbre  de  la class Huffman du fichier HuffmanV3"""
        #condition si choix = 1 alors on faire le etape par etape sinon on fait affichage direct donc on va afficher uniquement le resultat
        if choix == 1:
            self.arbre=self.class_H.arbre_huffman(self.seq,1)
            #reinitialise la liste one_liste au cas ou l'utilisateur veut aussi faire l'autre type d'affichage
            self.class_H.one_liste=[]
        
        else:
            self.arbre=self.class_H.arbre_huffman(self.seq,0)

            L_arbre=Label(frame,text=self.class_H.one_liste,fg="#578F97", font=("Georgia", 12))
            L_arbre.grid(row=row, column=4, sticky=W)
            #reinitialise la liste one_liste au cas ou l'utilisateur veut aussi faire l'autre type d'affichage
            self.class_H.one_liste=[]
        
        
        
    def Dico_cle(self,frame,row):
        """ fonction permettant d'appeler la fonction generant le dictionnaire de conversion  de  la class Huffman du fichier HuffmanV3"""
        #ici on stocke le dictionnairre de conversion qui va nous servir a coder et decoder la sequence 
        dico_cle=self.class_H.make_dico(self.arbre)
        
        L_Dico=Label(frame,text=dico_cle,fg="#578F97", font=("Georgia", 12))
        L_Dico.grid(row=row, column=4, sticky=W)
        
        
    def Encodage(self,frame,row):
        """ fonction permettant d'appeler la fonction permettant de passer de ADN a binaire  de  la class Huffman du fichier HuffmanV3"""
        #appel le dictionnaire de convertion
        dico_cle=self.class_H.make_dico(self.arbre)
        #appel la fonction qui permet de coder 
        self.seq_bin=self.class_H.encodage(self.seq,dico_cle)
        
 
        #affichage via la methode message de Tkinter comme ca on va pouvoir bien l'afficher dans l'application 
        #ATTENTION C EST LA SEQUENCE BINAIRE REPRESENTER MAIS SUR PLUSIEUR LIGNE POUR UNE QUESTION DE VISIBILITE
        L=Message(frame,text=self.seq_bin,fg="#578F97",font=("Georgia", 12))
        L.grid(row=row,column=4,sticky=W)

        
    def Decodage(self,frame,row,oui):
        """ fonction permettant d'appeler la fonction permettant de passer de binaire a ADN  de  la class Huffman du fichier HuffmanV3"""
        #appel le dictionnaire de convertion
        dico_cle=self.class_H.make_dico(self.arbre)
        
        #CETTE PARTIE EST DE LE DECODAGE 2 c'est a dire : sequence ADN -> binaire -> caractere -> binaire 
        #c'est le deuxieme décodage
        #en effet on est obliger d'appeler les fonction ici sinon probleme d'implémentation 
        self.caractere=self.class_H.one_char(self.seq_bin)
        self.back_to_bin=self.class_H.retour_binaire(self.caractere)

        #création d'une condition pour ne pas devoir crée deux fois la meme fonction
        if oui ==1:
            #donc ici c'est le sequence ADN -> binaire qu'on va decoder
            temp=self.seq_bin
        else:
            #donc ici c'est le sequence ADN -> binaire -> caractere -> binaire qu'on va decoder
            temp=self.back_to_bin
            #seq_ADN_back sera utiliser pour la sauvegarde dans le fichier
            self.seq_ADN_back=self.class_H.decodage(temp,dico_cle)
            
        #appel la fonction qui permet de decoder 
        L_Dico=Label(frame,text=self.class_H.decodage(temp,dico_cle),fg="#578F97" ,font=("Georgia", 12))
        L_Dico.grid(row=row, column=4, sticky=W)
        
        
    def one_char(self,frame,row):
        """ fonction permettant d'appeler la fonction permettant de passer de binaire a  caractere en 8 bits(octet)  de  la class Huffman du fichier HuffmanV3"""
        L=Label(frame,text=self.caractere, fg="#578F97",font=("Georgia", 12))
        L.grid(row=row, column=4, sticky=W)
        
    def one_char_to_bin(self):
        """ fonction permettant d'appeler la fonction permettant de passer de caractere a binaire  de  la class Huffman du fichier HuffmanV3"""
        #choisit d'afficher cette fonction dans un new onglet tkinter car c'est plus pratique et évite de surcharger l'application principale
        fInfos = Toplevel()		  
        fInfos.title('Huffman')                   
        fInfos.geometry("350x250") 
        
        #Message permet un affichage spécifique et non l'affichage sur une ligne 
        #C'EST LA MEMES SEQUENCES MAIS SUR PLUSIEURS LIGNE !
        LB=Message(fInfos,text=self.back_to_bin, fg="#578F97",font=("Georgia", 12))
        LB.grid(row=1, column=1, sticky=W)
        #bouton pour quitter le pop up
        B_quit=Button(fInfos, text='Quitter', command=fInfos.destroy)
        B_quit.grid(column=3) 

        

        
        
#sauvegarder temporairement pour reprendre 
#    def temp_file(sefl,frame,row,element_to_save):
#       """La sauvagarde et la reprise se fera a partir de la partie decodage/encodage
#            sinon va faloir efaire l'arbre (car complique de save autre chose que des type str)"""
#        f=open("temp.txt","w")
#        f.write(element_to_save)
#        f.close()
#
#        L=Label(frame,text="Fichier sauvegarder", font=("Georgia", 10),fg='white',bg='#D2C7DA')
#        L.grid(row=row, column=4, sticky=W)
#        


        
    def Output_file(self,frame,row):
        """Genere un fichier de sortie avec la sequence ADN et la sequence binaire  """
        f=open("save.txt","w")
        f.write("Sequence de depart : ")
        f.write(self.seq)
        f.write("\n \n")
        f.write("Sequence binaire : ")
        f.write(self.seq_bin)
        f.write("\n \n")
        f.write("Caracetre : ")
        f.write(self.caractere)
        f.write("\n \n")
        f.write("Sequence binaire apres caractere : ")
        f.write(self.back_to_bin)
        f.write("\n \n")
        f.write("Retour a la sequence de départ ")
        f.write(self.seq_ADN_back)
        
        f.close()

        L=Label(frame,text="Fichier sauvegarder", font=("Georgia", 12),fg='white',bg='#075C69')
        L.grid(row=row, column=4, sticky=W)
        

















 

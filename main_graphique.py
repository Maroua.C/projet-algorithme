#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 20:29:00 2020

@author: marwa

"""
#Import Tkinter neecessaire a l'affichage
from tkinter import filedialog
from tkinter import *
from tkinter import ttk

#importe les deux fichiers qui s'occupe dela partie graphique des deux algo
import BWT_G 
import Huffman_G 


class Application:
    
    def __init__(self):
        """Constructeur de la class Aplication """ 
        #############################
        ##définition de la fenetre###
        #############################
        self.window=Tk()	
        self.window.geometry("950x850")
        self.window.title("Mon application Compression")
        self.window.config(background='#BBC2E6')
             
        #############################
        ##__Variable necessaire__#### 
        #############################  
                 
        	#Initialisation pour les boutons pour BWT sera réaliser sous forme de dictionnaire 
        #1er element pour premiere etape BWT
        #2eme element pour deuxieme etape BWT.
        self.dico_checkbox = {
            'BWT_1_direct':IntVar(),
            'BWT_1_etape': IntVar(),
            'BWT_2_direct': IntVar(),
            'BWT_2_etape': IntVar()
        }     
        
        #importe la class BWT et Huffman pour pouvoir faire interface graphique
        self.Graphic_BWT=BWT_G.BurrowsWeeler_Graph()
        self.Graphic_Huffman=Huffman_G.Huffman_graph()
     
        #############################
        ##définition des onglets  ###
        #############################
        #on va crée 4 onglet differents dans un meme tk() qui aura 4 frames dfferente 
        onglet_tk = ttk.Notebook(self.window)
        #introduction
        self.tab1 = Frame(onglet_tk)
        #Partie BWT
        self.tab2 = Frame(onglet_tk)
        #Partie Huffman
        self.tab3 = Frame(onglet_tk)
        #Partie Humman+BWT
        self.tab4 = Frame(onglet_tk)
        
        #juste les titres des onglets 
        onglet_tk.add(self.tab1, text='Intro')
        onglet_tk.add(self.tab2, text='BWT')
        onglet_tk.add(self.tab3, text='Huffman')
        onglet_tk.add(self.tab4, text='BWT+Huffman')
        onglet_tk.pack(expand=1, fill='both')
        
        #fonction pour initialiser les commandes tkinter.
        self.cree_widget()

 
    def cree_widget(self):
        """Toute les fonctions appeler ici sera appeler directement quand l'application sera lancée """
        #############################
        ##____Intruction_________ ###
        #############################
        #introduction c'est a dire le premier onglet explicatif 
        self.Label_titre_intro()
        self.Label_sous_titre_intro()
        self.Canvas_texte_intro()
        
        
        #############################
        ##___________BWT_________ ###
        #############################
        #Contient les labels de BWT 
        self.BWT_Label()        
        self.input_file(self.tab2,2,3)


        
        #############################
        ##________Huffman_________###
        #############################
        self.Huffman_Label()
        self.Huffman_create_bouton_file()

        #############################
        ##______BWT + Huffman_____###
        #############################
        self.title_BWTHuffman()
        self.BWT_affichage1()


################################################################################################################## 
#ONGLET INTRODUCTION
       
    
    def Label_titre_intro(self):
        """Fonction contenant le label qui fait offcie de titre de l'application """
        L=Label(self.tab1,text="Bienvenue dans l'interface de compression",font=("Georgia", 30),fg='#075C69')
        L.pack()
        
    def Label_sous_titre_intro(self):
        """Fonction contenant le label qui fait offcie de sous titre de l'application """

        L=Label(self.tab1,text="Veuillez choisir parmi les onglets ",font=("Georgia", 15),fg='#075C69')
        L.pack()
        
    def Canvas_texte_intro(self):
        """Fonction contenant toute la partie explicative de l'application"""
        texte =Text(self.tab1, wrap='word')
        texte.pack()
 
        CONTENU="Cette application propose 3 possibilitée:\nTout d'abord BWT qui permet à partir d'une sequence ADN de transformer cette sequence en BWT on va alors obtenir une sequence BWT puis celle ci pourra être décompresser .  On va alors revenir à une séquences ADN.\n \nPuis un second onglet qui permet d'utiliser Huffman et de transformer la sequence ADN en binaire -> caractere .On peut également faire :séquence ADN -> binaire -> caractere -> binaire -> sequence ADN.\n\nEnfin un dernier onglet qui permet de faire les deux algorithmes en même temps.  "
        texte.insert('1.0', CONTENU)
        L_titre=Label(self.tab1,text="REMARQUE:\n On a en input un fichier contenant la sequence uniquement",fg="#578F97",font=("Georgia", 12))
        L_titre.pack()

 
################################################################################################################## 
#ONGLET BWT
        
    def BWT_Label(self):
        """ juste une fonction qui regroupe les labels pour faire jolie"""
        #Titre de la partie ici on est dans BWT 
        L_titre=Label(self.tab2,text="Partie BWT",fg="#075C69",font=("Georgia", 30))
        L_titre.grid(row=1, column=2, sticky=E)
        
         
        L_import_file=Label(self.tab2,text="Importation du fichier:" ,fg="#578F97",font=("Georgia", 12))
        L_import_file.grid(row=2, column=1, sticky=W)
        
      
    def input_file(self,frame,row,col):
        """Boutons qui permet d'ajouter un fichier """
        bouton_file=Button(frame, text="Input sequence",width=20, command=lambda :self.fusion_seqADN(frame))
        bouton_file.grid(row=row, column=col, sticky=W)
        
    
    def fusion_seqADN(self,frame):
        """Cette fonction s'apelle fusion car elle va appeler plusieur autre fonction en meme temps (juste pour la visibilité) """
        #Deja on appeler la foncion qui permet d'obtenir le chemin du fichier
        self.Graphic_BWT.BWT_get_path_fichier()

        #condition si la sequence contient un '$' cad caractere spécial alors c'est une sequence BWT (transformer par algorithme BWT)
        if '$' in self.Graphic_BWT.seqfromfile:
            #sera decommenter quand partie sequence BWT input traiter 
            #self.BWT_step2()
            L=Label(frame,text="C'est une sequence BWT", font=("Georgia", 12),fg='white',bg='#075C69')
            L.grid(row=5, column=3, sticky=W+E)
            L=Label(frame,text="Veuillez renseigner séquence ADN", font=("Georgia", 12))
            L.grid(row=6, column=3, sticky=W+E)

        else:
            #sinon on peut supposer que c'est une sequence ADN en esperant que l'utilisateur à bien rentrer une sequence composer uniquement de ACTGN
            self.BWT_step1()
            L=Label(frame,text="C'est une séquence ADN", font=("Georgia", 12),fg='white',bg='#075C69')
            L.grid(row=5, column=3, sticky=W+E)
            #juste pour une question graphique j'ai mis le label ci dessous en blanc
            L=Label(frame,text="Veuillez renseigner sequence ADN",fg='white', font=("Georgia", 12))
            L.grid(row=6, column=3, sticky=W+E)
            #Appel de la fonction qui permet de generer les checkbox 
            self.validate_check_bouton(frame,7,4)

########################################
######_______BWT etape 1________########
########################################             

    def BWT_step1(self):
        """Etape 1 du BWT cad sequence de depart -> sequence BWT  """
  
        L_step1=Label(self.tab2,text="Etape 1 (ADN > BWT):",fg="#075C69", font=("Georgia", 12))
        L_step1.grid(row=7, column=1, sticky=W)

        #check bouton
        #choisir d'afficher soit etape par etape soit le resultat directement
        check_etape=Checkbutton(self.tab2,variable=self.dico_checkbox['BWT_1_etape'],onvalue=1,offvalue=0,height=2,width=2)
        check_etape.grid(row=7,column=3, sticky=E)
        
        check_direct=Checkbutton(self.tab2,variable=self.dico_checkbox['BWT_1_direct'],onvalue=1,offvalue=0,height=2,width=2)
        check_direct.grid(row=8,column=3, sticky=E)
		
        
        #Label permettant de savoir quelle checkbos correspond a quelle options 
        L_etape=Label(self.tab2,text="Step by step",fg="#578F97", font=("Georgia", 12))
        L_etape.grid(row=7,column=2, sticky=W)
        
        L_direct=Label(self.tab2,text="Direct",fg="#578F97", font=("Georgia", 12))
        L_direct.grid(row=8, column=2, sticky=W)
		
########################################
######_______BWT etape 2________########
########################################  
    
    def BWT_step2(self): 
        """Etape 2 du BWT cad sequence BWT -> sequence depart  """

        L_step2=Label(self.tab2,text="Etape 2 (BWT -> ADN) :",fg="#075C69", font=("Georgia", 12))
        L_step2.grid(row=10, column=1, sticky=W)
 
        #check bouton
        #choisir d'afficher soit etape par etape soit le resultat directement
        check_etape=Checkbutton(self.tab2,variable=self.dico_checkbox['BWT_2_etape'],onvalue=1,offvalue=0,height=2,width=2)
        check_etape.grid(row=10,column=3, sticky=E)
        
        check_direct=Checkbutton(self.tab2,variable=self.dico_checkbox['BWT_2_direct'],onvalue=1,offvalue=0,height=2,width=2)
        check_direct.grid(row=11,column=3, sticky=E)
        
        #Label
        L_etape=Label(self.tab2,text="Step by step",fg="#578F97", font=("Georgia", 12))
        L_etape.grid(row=10,column=2, sticky=W)
		
        L_direct=Label(self.tab2,text="Direct",fg="#578F97", font=("Georgia", 12))
        L_direct.grid(row=11, column=2, sticky=W)


########################################
######_______partie checkbox________####
########################################         
    def BWT_get_valeurs_checkbox(self):
        """fonction permettant d'exe une fonction specifique en fonction du ce que utilisateur a cocher """
       
        #print c'est juste une verification dans le terminal pour voir si le get a bien ete fait 
        print("valeur checkbox sont :")	
		#rempli le dico en fonction du checkbox 				
        for key,val in self.dico_checkbox.items():
            #ICI, je récupère les valeurs
            val = val.get() 
            print('KEY:', key, 'VAL:', val)
	
            if key =='BWT_1_direct' and val == 1:	
                #ici on est dans la premier etape et on affiche le reultat directement 
                L=Label(self.tab2,text="Vous avez choisit direct 1", font=("Georgia", 12),fg='white',bg='#075C69')
                L.grid(row=9, column=3, sticky=W+E)
                
                #on met 0 a self.Graphic_BWT.choix_step1 car on veut l'étape 1 direct cette variable sera utiliser dans la fonction self.Graphic_BWT.popup_step1()
                self.Graphic_BWT.choix_step1= 0
                #etape 1 du BWT
                self.Graphic_BWT.popup_step1()
                #affiche les options pour la seconde etape
                self.BWT_step2()
	
            elif key =='BWT_1_etape' and val == 1:	
                #ici on est dans la premier etape et on affiche le reultat etape par etape 
                L=Label(self.tab2,text="Vous avez choisit etape 1", font=("Georgia", 12),fg='white',bg='#075C69')
                L.grid(row=9, column=3, sticky=W+E)
                
                #on met 1 a self.Graphic_BWT.choix_step1 car on veut l'etape 1 etape par etape cette variable sera utiliser dans la fonction self.Graphic_BWT.popup_step1()
                self.Graphic_BWT.choix_step1= 1
                self.Graphic_BWT.choix_step2= 0
                #etape 1 du BWT
                self.Graphic_BWT.popup_step1()
                #affiche les options pour la seconde etape
                self.BWT_step2()
            
            elif key =='BWT_2_etape' and val == 1:	
                #ici on est dans la deuxieme etape et on affiche le reultat etape par etape 
                L=Label(self.tab2,text="Vous avez choisit etape 2", font=("Georgia", 12),fg='white',bg='#075C69')
                L.grid(row=12, column=3, sticky=W+E)
                
                
                #on met 1 a self.Graphic_BWT.choix_step2 car on veut l'etape 2 etape par etape cette variable sera utiliser dans la fonction self.Graphic_BWT.popup_step2()
                self.Graphic_BWT.choix_step2= 1
                # initialise self.Graphic_BWT.choix_step1 a 0 car si l'utilisateur a fait etape par etape alors il sera a 1 et on aura l'affichage de etape par etape de l'etape 1 hors on ne veut pas 
                self.Graphic_BWT.choix_step1= 0
                #etape 2 du BWT
                self.Graphic_BWT.popup_step2()
                
            elif key =='BWT_2_direct' and val == 1:	
                #ici on est dans la deuxieme etape et on affiche le reultat direct
                L=Label(self.tab2,text="Vous avez choisit direct 2", font=("Georgia", 12),fg='white',bg='#075C69')
                L.grid(row=12, column=3, sticky=W+E)

                #on met 0 a self.Graphic_BWT.choix_step2 car on veut l'etape 2 direct cette variable sera utiliser dans la fonction self.Graphic_BWT.popup_step2()
                self.Graphic_BWT.choix_step2=0
                #etape 2 du BWT
                self.Graphic_BWT.popup_step2()            
            
    def validate_check_bouton(self,frame,row,col):
        """submit prend en commande la fonction BWT_get_valeurs_checkbox """
        #donc c'est pour valider le choix sur les checkbox que l'utilisateur aura fait et ainsi faire l'affichage approprié
        submit_all = Button(frame,text="Submit ",width=10,command=self.BWT_get_valeurs_checkbox)
        submit_all.grid(row=7, column=4, sticky=W)
		

################################################################################################################## 
#ONGLET Huffman 
        
    def aspect_graphique(self,row,col):
        """Cette fonction sert juste à espacer les options dans huffman c'est un label avec de l'ecriture blanche alors on va rien voir dans l'appli """
        invisible=Label(self.tab3,text="Juste pour sauter une ligne",fg="white",font=("Georgia", 15))
        invisible.grid(row=row, column=col, sticky=E)
        
        
    def Huffman_Label(self):
        """ juste une fonction qui regroupe le labels pour faire jolie"""
        
        L_titre=Label(self.tab3,text="Partie Huffman",fg="#075C69",font=("Georgia", 30))
        L_titre.grid(row=1, column=2, sticky=E)
        
      
    def fusion_fonction(self,frame):
        """creation de cette methode pour que quand utilisateur  appuis sur boutton le reste s'affiche juste pour que ca soit interactive """        
        self.Graphic_Huffman.Huffman_get_path_fichier(frame)
        self.Huffman_Table_frequence()
        self.Huffman_Arbre()

        
    def Huffman_create_bouton_file(self):
        """ Creation d'un bouton ou utilisateur pourra mettre le fichier désirer"""
        L_import_file=Label(self.tab3,text="Importation du fichier:",fg="#075C69", font=("Georgia", 12))
        L_import_file.grid(row=2, column=1, sticky=W)
        
        bouton_file=Button(self.tab3, text="Import",width=10, command=lambda:self.fusion_fonction(self.tab3))
        bouton_file.grid(row=2, column=4, sticky=W)
        self.aspect_graphique(3,2)
        
        
    def Huffman_Table_frequence(self):
        """Fonction permettant d'afficher la table/dictionnaire des frequences  """
        L_frequence=Label(self.tab3,text="Table de fréquence",fg="#075C69", font=("Georgia", 12))
        L_frequence.grid(row=4, column=1, sticky=W)
        
        bouton_file=Button(self.tab3, text="Ok",width=10, command=lambda :self.Graphic_Huffman.Frequence(self.tab3,5))
        bouton_file.grid(row=4, column=4, sticky=W)
        self.aspect_graphique(5,2)
        
    def fusion_fonction2(self,frame,row,choix):
        """creation de cette fonction pour que quand use appuie sur le bouton, le reste s'affiche juste pour que ca soit interactive 
        parce que faut faire l'arbre avant"""
        #ici on va alors mettre les fonctions qui affiche l'arbre l'encodage , le decodage et le l'affichage de la sequence en caractère
        self.Huffman_Dico_cle(self.tab3,12,1)
        self.Huffman_encodage(self.tab3,16,1)
        self.Huffman_decodage(self.tab3,20,1,1)
        self.Huffman_one_char(frame,24,1)


        #on va appeler la fonction Arbre du fichier Graphic_Huffman 
        self.Graphic_Huffman.Arbre(frame,row,choix)
        #fonction qui permet de sauvegarder la sequence ADN et le binaire, et la sequence en caractère dans un fichier txt
        self.Huffman_save_final()
        

    def Huffman_Arbre(self):
        """Fonction permettant d'afficher l arbre  de huffman """

        L_frequence=Label(self.tab3,text="L'abre de Huffman", fg="#075C69",font=("Georgia", 12))
        L_frequence.grid(row=6, column=1, sticky=W)
        
        bouton_file=Button(self.tab3, text="Direct",width=10, command=lambda :self.fusion_fonction2(self.tab3,7,0))
        bouton_file.grid(row=6, column=4, sticky=W)
                
        bouton_file1=Button(self.tab3, text="Etape",width=10, command=lambda :self.fusion_fonction2(self.tab3,10,1))
        bouton_file1.grid(row=9, column=4, sticky=W)
        
        self.aspect_graphique(11,2)
        
        
    def Huffman_Dico_cle(self,frame,row,col):
        """Fonction permettant d'afficher le dictionnaire de convertion en utilisant le fichier importer Huffman_G baptiser Graphic_Huffman """
       
        L_dico=Label(self.tab3,text="Dictionnaire pour crypter/Décrypter",fg="#075C69", font=("Georgia", 12))
        L_dico.grid(row=row, column=col, sticky=W)
        
        bouton_file=Button(self.tab3, text="Dico",width=10, command=lambda :self.Graphic_Huffman.Dico_cle(self.tab3,row+1))
        bouton_file.grid(row=row, column=col+3, sticky=W)
        self.aspect_graphique(row+2,2)

        
    def Huffman_encodage(self,frame,row,col):
        """Fonction permettant d'afficher le passage de ADN a sequence binaire en utilisant le fichier importer Huffman_G baptiser Graphic_Huffman """

        L_E=Label(frame,text="Crypter la sequence ",fg="#075C69", font=("Georgia", 12))
        L_E.grid(row=row, column=col, sticky=W)
        
        bouton_file=Button(frame, text="Encode",width=10, command=lambda :self.Graphic_Huffman.Encodage(frame,row+1))
        bouton_file.grid(row=row, column=col+3, sticky=W)
        self.aspect_graphique(row+2,2)

         
    def Huffman_decodage(self,frame,row,col,oui):
        """Fonction permettant d'afficher le passage  sequence binaire a ADN en utilisant le fichier importer Huffman_G baptiser Graphic_Huffman """

        L_E=Label(frame,text="Decrypter la sequence ",fg="#075C69", font=("Georgia", 12))
        L_E.grid(row=row, column=col, sticky=W)
        
        bouton_file=Button(frame, text="Decode",width=10, command=lambda :self.Graphic_Huffman.Decodage(frame,row+1,oui))
        bouton_file.grid(row=row, column=col+3, sticky=W)
        
    
#    def Huffman_file(self,element_to_save):
#        """ faire des sauvegarde temporaire pour quand on quitte appli""" 
#        L_save=Label(self.tab3,text="Sauvagarder pour reprendre ", font=("Georgia", 12))
#        L_save.grid(row=16, column=1, sticky=W)
#        
#        bouton_file=Button(self.tab3, text="Save",width=10, command=lambda :self.Graphic_Huffman.temp_file(self.tab3,17,element_to_save))
#        bouton_file.grid(row=16, column=4, sticky=W)
        
    def Huffman_one_char(self,frame,row,col):
        """Fonction permettant d'afficher le passage de sequence binaire a caractere en 8bits en utilisant le fichier importer Huffman_G baptiser Graphic_Huffman """

        L_E=Label(frame,text="Ecrire la sequence sur un caractere : ", fg="#075C69",font=("Georgia", 12))
        L_E.grid(row=row, column=col, sticky=W)
        
        bouton_file=Button(frame, text="Caractere",width=10, command=lambda :self.Graphic_Huffman.one_char(frame,row+1))
        bouton_file.grid(row=row, column=col+3, sticky=W)
        
        self.aspect_graphique(row+2,2)
        #appel la fonction qui permet de passer a caractére -> binaire
        self.Huffman_one_char_to_bin(frame,row+2,1)
        
    def Huffman_one_char_to_bin(self,frame,row,col):
        """ prend le caractere et le convertie en binaire """
                
        L_E=Label(frame,text="Caractere -> binaire: ", fg="#075C69",font=("Georgia", 12))
        L_E.grid(row=row, column=col, sticky=W)
        
        bouton_file=Button(frame, text="Binaire",width=10, command=self.Graphic_Huffman.one_char_to_bin)
        bouton_file.grid(row=row, column=col+3, sticky=W)
        #afficher la fonction decodage encore une fois mais comme le dernier paramètre est 0 alors ca va etre la seq ADN -> bin -> caractere -> bin qu'on va afficher 
        self.Huffman_decodage(frame,row+2,1,0)
        
        
    def Huffman_save_final(self): 
        """Fonction permettant de sauvegarder les sequences en utilisant le fichier importer Huffman_G baptiser Graphic_Huffman"""
        self.aspect_graphique(40,2)

        L_save=Label(self.tab3,text="Sauvagarder ", fg="#075C69",font=("Georgia", 12))
        L_save.grid(row=41, column=1, sticky=W)
        bouton_file=Button(self.tab3, text="Ok",width=10, command=lambda :self.Graphic_Huffman.Output_file(self.tab3,42))
        bouton_file.grid(row=41, column=4, sticky=W)
     
 
################################################################################################################## 
#ONGLET BWT + Huffman         
        
    def title_BWTHuffman(self):
        """Label tkinter pour la partie graphique """
        L_titre=Label(self.tab4,text="Partie BWT + Huffman ",fg="#075C69",font=("Georgia", 30))
        L_titre.grid(row=1, column=2, sticky=E)

        L_BWT=Label(self.tab4,text=" BWT  ",fg="#075C69",font=("Georgia", 20))
        L_BWT.grid(row=2, column=1, sticky=E)

########################################
######_______BWT____________________####
########################################         
    def fusion_pour_partie0(self):
        """ je regroupe des fonctions ici pour une question graphique comme ca quand utilisateur appuie sur le bouton 
            on aura un message comme quoi on a submit le fichier"""
            
        #Je n'ai pas fait la partie si utilisateur met sequence BWT car deja abordée dans la partie BWT seul
        self.Graphic_BWT.BWT_get_path_fichier()
        
        L=Label(self.tab4,text="Vous avez rentré la sequence ", font=("Georgia", 12),fg='white',bg='#075C69')
        L.grid(row=5, column=3, sticky=W+E)
        
    def BWT_affichage1(self):
        """Input file pour BWT """
        L_import_file=Label(self.tab4,text="Importation du fichier:",fg="#075C69", font=("Georgia", 12))
        L_import_file.grid(row=3, column=1, sticky=W)
        
        bouton_file=Button(self.tab4, text="input seq ADN ",width=20, command=self.fusion_pour_partie0 )
        bouton_file.grid(row=3, column=3, sticky=W)
        
        #J'ai fait que la partie direct car la partie etape par etape à deja ete fait dans la partie BWT seul
        L_step1=Label(self.tab4,text="Etape 1 (ADN > BWT):",fg="#075C69", font=("Georgia", 12))
        L_step1.grid(row=6, column=1, sticky=W)
        
        B_1=Button(self.tab4, text="submit step1",width=20, command= self.BWT_partie1)
        B_1.grid(row=6, column=3, sticky=W)
        
    def BWT_partie1(self):  
        """ BWT partie une """
        self.Graphic_BWT.choix_step1= 0
        self.Graphic_BWT.choix_step2=0
                
        self.Graphic_BWT.popup_step1()
        self.BWT_affichage2()
        
    def BWT_partie2(self):
        """ BWT partie deux """
        self.Graphic_BWT.choix_step1=0
        self.Graphic_BWT.choix_step2=0
        self.Graphic_BWT.popup_step2()
        
        #appel de cette fonction pour pouvoir passer à la partie huffman 
        self.Huffmantitle()
        
    def BWT_affichage2(self):
        """ BWT partie deux affichage """
        L_step1=Label(self.tab4,text="Etape 2 (BWT > ADN):", fg="#075C69",font=("Georgia", 12))
        L_step1.grid(row=7, column=1, sticky=W)
        
        B_1=Button(self.tab4, text="submit step2",width=20, command= self.BWT_partie2)
        B_1.grid(row=7, column=3, sticky=W)
        
########################################
######_______Huffman________________####
########################################         
    def Huffmantitle(self):
        """ mtn on va faire la partie huffman une fois que BWT a ete fait  """
        
        L_Huffman=Label(self.tab4,text=" Huffman  ",fg="#578F97",font=("Georgia", 20))
        L_Huffman.grid(row=8, column=1, sticky=E)
        
        #ici on va implementer la sequence travailler dans l'algo BWT dans la variable self.Huffman_graph.seq pour pouvoir lancer l'algo Huff
        self.Graphic_Huffman.seq=self.Graphic_BWT.seqfromfile
        self.Huffman_algo()

    def Huffman_algo(self):
        """La fonction contient toute la partie Huffman qui sera faite aprés BWT ,
        J'ai choisie d'afficher uniquement l'arbre etape par étape et non directement la derniere implementation de l'arbre
        et je restraint les boutons aux codage decodage car le huffman plus detailler est dans l'onglet ou il y a huffman uniquement"""
        L_frequence=Label(self.tab4,text="L'abre de Huffman",fg="#578F97", font=("Georgia", 12))
        L_frequence.grid(row=9, column=1, sticky=W)
        
        self.Graphic_Huffman.Import_classHUFF(self.tab4)
        #ici je choisit d'afficher que le etape par etape car visuellement c'est meilleurs a voir que l'affichage uniquement de la derniere etape de huffmn
        bouton_file=Button(self.tab4, text="Etape par Etape",width=20, command=lambda :self.Graphic_Huffman.Arbre(self.tab4,9,1))
        bouton_file.grid(row=9, column=3, sticky=W)
        
        self.Huffman_Dico_cle(self.tab4,10,1)
        self.Huffman_encodage(self.tab4,12,1)
        self.Huffman_decodage(self.tab4,14,1,1)
        self.Huffman_one_char(self.tab4,16,1)




       

My_first_app=Application()
My_first_app.window.mainloop()	

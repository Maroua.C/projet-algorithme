#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 23:34:15 2020

@author: marwa
"""
#############################################################################################
#________________Fichier qui contient la partie graphique du code BWT ______________________# 
#############################################################################################

#Importation de tkinter 
from tkinter import *
from tkinter import ttk

#importe le fichier qui contient le code BWT
import BWT

class BurrowsWeeler_Graph:
    def __init__(self):
        """Constructeur de la classe BurrowsWeeler_Graph """
        
        #stocke le chemin du fichier
        self.filename=""
        
        #contient la sequence qu'on a importer du fichier 
        self.seqfromfile=""
        
        #si user a rentrer une sequence BWT c'est a dire avec un caractere spéciale '$'
        self.input_file_BWT=""
        
        #stocke la sequence BWT en str issu de l'étape 1 
        self.only_BWT=""
        
        #pour un affichage direct ou step by step: 0 direct et 1 step by step
        #ici on les initialise a 0 juste pour les déclarer 
        self.choix_step1=0
        self.choix_step2=0
        
        #on les met en variables global de classe  car on va les utiliser plusieurs fois dans le code 


######################################
######_____Input fichier______########
######################################        
        
    def BWT_get_path_fichier(self):
        """ fonction qui permet d'obtenir le chemin du fichier """
        
        self.filename =filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("Text Files", "*.txt"),("all files","*.*")))        
        #print(self.filename)
        
        #ouverture du fichier que l'utilisateur vient de charger 
        f=open(self.filename,'r')
        f=f.read()
        #Commande pour virer les \n (rstrip)
        self.seqfromfile=f.rstrip() 
        
#        #cette condtition veut dire qu'on a rajouter une sequence BWT directement
#        if '$' in self.seqfromfile:
#            #on va alors mettre la sequence dans un variable de classe qu'on va utiliser plus tard pour convertir 
#            #convertion en liste necessaire pour les manipulation suivante
#            self.input_file_BWT=list(self.seqfromfile)
        print(type(self.seqfromfile))
        
        return self.seqfromfile


##################################################################################################################        
#sequence DNA -> sequence BWT
        
########################################
######_______BWT etape 1________########
######################################## 
        
    def BurrowsWeeler_step0(self):
        """ Etape 0 , en effet on va appeler la class BWT"""
        #Apelle la fonction pour lire un fichier 
        seq=self.seqfromfile

        #stock dans une variable la classe (c'est le fichier qui contient le code BWT)
        self.class_bwt=BWT.BurrowsWeeler(seq)
        
        #retourne seq pour pouvoir l'utiliser apres 
        return seq
        

    def BurrowsWeeler_step1(self,frame):
        """ appel de etape 0 (pour appeler la classe), et on appeler les methode cette meme classe"""
        #stocke la sequence d'interet pour pouvoir la reutiliser         
        seq=self.BurrowsWeeler_step0()
        
        #appel cette fonction pour rajouter un '$'
        seq_list=self.class_bwt.convert_list(seq) 
        
        #creation d'une matrice numpy vide ayant les dimenssions de la lg de la seq 
        matrix=self.class_bwt.create_empy_matrix(self.class_bwt.len_seq,self.class_bwt.len_seq)
        
        #Cette condition sera vrai quand on va faire le etape par etape 
        if self.choix_step1==1:
            #remplit la sequence en decalant de un le $ donc la matrice sera remplie
            #comme le 3 eme parametre de la fonction appeler est 1 alors l'affichage sera etape par etape
            self.class_bwt.fill_matrix(seq_list,matrix,1,frame)
        
        #l'afichage sera direct car troisieme parametre est égale a 0 
        self.matrix_filled=self.class_bwt.fill_matrix(seq_list,matrix,0,frame)

        #ici on trie la matrice 
        sorted_matrix=self.class_bwt.lexicography_sort(self.matrix_filled)  
        
        #pour pouvoir afficher uniquement  la sequence BWT (pour des question graphique)
        self.only_BWT=self.class_bwt.get_BWTseq(sorted_matrix)
            
        return sorted_matrix  
    


#TKINTER PART              
    def popup_step1(self):
        """ Prend etape 1  qu'on va mettre dans des labels pour les afficher sur tkinter"""
        #variable qui permettera d'adapter la fenetre en fonction de l'option direct ou étpe par étape
        dimension=""
        #creation d'un pop up car on va ouvrir dans des new fenetre d'ou utilisation de la 
        #methode Toplevel()
        fInfos = Toplevel()		  
        fInfos.title('BWT etape 1')
        fInfos.geometry(dimension) 
        
        #on a besoin des variables retourner par ces fonctions pour pouvor les afficher
        seq=self.BurrowsWeeler_step0()
        
        #affiche la sequence 
        L_txt_seq=Label(fInfos,text="The sequence",fg="#578F97",font=("Georgia", 12))
        L_txt_seq.grid(row=1, column=2)
        L_seq=Label(fInfos,text=seq,fg="#578F97",font=("Georgia", 12))
        L_seq.grid(row=1, column=3)

        #trie lexicographiqement la sequence
        #Le label L2 est en ecriture blanche juste pour une question graphique comme ca c'est plus aérer   
        L2=Label(fInfos,text="MATRIX LEXICOGRAPHY SORT",fg="white",font=("Georgia", 20))
        L2.grid(row=3, column=3) 
        
        if self.choix_step1 == 0:
            dimension="900x450"
            #juste pour aspect graphique comme ca le step by step ne s'affiche pas quand on fait le mode direct 
            L3=Label(fInfos,text="STEP BY STEP",fg="white",font=("Georgia", 15))
      
        else:
            dimension="1200x450"
            L3=Label(fInfos,text="STEP BY STEP",fg="#075C69",font=("Georgia", 15))
        
        L3.grid(row=4, column=1) 
        matrix_step2=self.BurrowsWeeler_step1(fInfos)
        
        #Resultat final on a trier la matrice en plus 
        L4=Label(fInfos,text="RESULTAT FINAL",fg="#075C69",font=("Georgia", 15))
        L4.grid(row=4, column=3) 
        msg2=Message(fInfos,fg="#075C69", text=matrix_step2)
        msg2.grid(row=5, column=3)
        
        
        #affiche la sequence BWT uniquement
        L3=Label(fInfos,text="BWT sequence ",fg="#578F97",font=("Georgia", 12))
        L3.grid(row=7, column=2)
        L_BWT=Label(fInfos,text=self.only_BWT,fg="#578F97",font=("Georgia", 12))
        L_BWT.grid(row=7, column=3)
        
        #bouton pour quitter le pop up
        B_quit=Button(fInfos, text='Quitter', command=fInfos.destroy)
        B_quit.grid(row=10, column=2)

        
##################################################################################################################        
#sequence BWT -> sequence DNA
        
########################################
######_______BWT etape 2________########
######################################## 
        
    def BurrowsWeeler_step2(self,frame):
        """ Etape 2 car on a la sequence BWT et on veut retrouver le sequence de depart """
        self.BurrowsWeeler_step0()
        
        #partie sequence BWT [EN COURS...]
#        if len(self.input_file_BWT) != 0:
#            matrix_step2=self.input_file_BWT
#            new_sorted_matrix=self.class_bwt.create_empy_matrix(len(matrix_step2),len(matrix_step2))
#
#
#        else:
        
        #on a besoin des donnes generer dans l'étape une pour pouvoir faire l'etape deux     
        matrix_step2=self.BurrowsWeeler_step1(frame)
        
        #on va aussi avoir besoin d'une nouvelle matrice c'est celle la qu'on va implementer 
        #on la transpose aussi pour pouvoir l'implémenter ensuite
        new_sorted_matrix=self.class_bwt.create_empy_matrix(self.class_bwt.len_seq,self.class_bwt.len_seq)
        new_sorted_matrix.transpose()

        #juste un label pour affciher le texte
        L2=Label(frame,text="MATRIX RECONSTRUITE",fg="#075C69",font=("Georgia", 15))
        L2.grid(row=2,column=1)
        
        #step by step ou direct ? si c'est 1 alors etape par etape si c'est 0 alors on affiche directement le resultat 
        if self.choix_step2 == 1:
            #etape par etape
            rebuild_matrix=self.class_bwt.Rebuild_matrix(new_sorted_matrix,matrix_step2,1,frame)
            
        else:
            #Affichage direct
            rebuild_matrix=self.class_bwt.Rebuild_matrix(new_sorted_matrix,matrix_step2,0,frame)
            msg=Message(frame,fg="#075C69", text=rebuild_matrix)
            msg.grid(row=2,column=2)

        return rebuild_matrix
    
    def BurrowsWeeler_final_step(self,frame):
        """ La derniere etape est d'afficher la sequence initiale """        
        #on a besoin de l'etape precedante pour pouvoir utiliser la matrice generer par celle ci     
        new_sorted_matrix=self.BurrowsWeeler_step2(frame)
               
        #BWT_final extrait la sequence de depart a partir de la matrice(celle qu'on vient de cree a partir de a BWT sequence)
        back_seq_list=self.class_bwt.BWT_final(new_sorted_matrix)
        
        #condition si back_seq_list n'est pas vide donc si on a fait l'etape deux alors on affiche la sequence 
        #la sequence afficher sera celle de depart         
        if back_seq_list != None:
            #TKINTER  PART      
            L2=Label(frame,text="GET THE SEQUENCE BACK ",fg="#578F97",font=("Georgia", 12))
            L2.grid(row=4,column=1)
            
            #ici on affiche la sequence ADN retrouver 
            msg=Label(frame,fg="#578F97", text=back_seq_list)
            msg.grid(row=4,column=2)
            
        

    def popup_step2(self):
        #pop up pour l'etape 2 c'est a dire l'onglet flottant
        fInfos = Toplevel()		  
        fInfos.title('BWT etape 2')
        fInfos.geometry("700x350") 
           
        #TKINTER PART 
        L3=Label(fInfos,text="BWT sequence ",fg="#578F97",font=("Georgia", 12))
        L3.grid(row=1,column=1)
        #ici on affiche la sequence BWT
        L_BWT=Label(fInfos,text=self.only_BWT,fg="#578F97",font=("Georgia", 12))
        L_BWT.grid(row=1, column=2)
        
        #ici on affiche le processus soit directement resultat final soit etape par etape 
        self.BurrowsWeeler_final_step(fInfos)
        
        #bouton pour quitter le pop up
        B_quit=Button(fInfos, text='Quitter', command=fInfos.destroy)
        B_quit.grid(row=10, column=1)

      
       
        
# Projet Algorithme 
# Transformée de Burrows Weeler et Compression d’Huffman


## Installation des librairies 

1.  heapq
2.  numpy
3.  tkinter

Utiliser le package [pip](https://pip.pypa.io/en/stable/) pour installer les librairies nécessaire.
```bash
pip install tkinter
pip install heapq
pip install numpy
```

## Explication des fichiers
Le projet se décompose en cinq fichiers *pour éviter de faire des fichiers trop longs*: 

*  main_graphique.py qui contient le main du projet 
    * Il contient la partie affichage en tkinter des algorithmes.
    * Il va alors afficher une fênettre tkinter composée de quatres onglets (une introduction/presention,Transformée de Burrows Weeler, Compression d’Huffman enfin les deux sucessivement ).

*  BWT.py
    * Il contient le code source du code BWT.

*  BWT_G.py
    * Il contient le nécessaire pour afficher le code BWT sur l'application en tkinter.

*  HuffmanV3.py
    * Il contient le code source du code Huffman.

*  Huffman_G.py
    * Il contient le nécessaire pour afficher le code Huffman sur l'application en tkinter.

**Description de chaque fichier**  

## main_graphique.py
Il est important de faire des importations en amont.

```python 
#Import Tkinter neecessaire a l'affichage
from tkinter import filedialog
from tkinter import *
from tkinter import ttk

#importe les deux fichiers qui s'occupe de la partie graphique des deux algorithmes.
import BWT_G 
import Huffman_G 
```
Le fichier contient une seule class en POO et se compose en quatre parties :

*  1er onglet  : Introduction de l'application 
*  2eme onglet : 
    * BWT qui va appeler les méthodes du fichier BWT_G.py.On va aussi avoir des checks boutons(les checks boutons sont implémentés grâce à un dictionnaire ) pour permettre à l'utilisateur de choisir l'affichage (soit il affiche directement le résultat et/ou il affiche toutes les étapes menant au résultat )
    * Il va tout d'abord falloir charger un fichier de type texte contenant une sequence ADN.
    * Suite a cela l'application va afficher la premiere partie du BWT avec des checks boutons (en fonction du choix on va avoir des fenètre flottante/pop up adéquats).
    * Une fois réalisé la 2ème partie du BWT va pouvoir être fesable avec le même principe de checks boutons.
    * Un message s'affichera dans l'application en fonction des choix réalisés .Exemple :"Vous avez choisi étape 2" Cela veut dire que l'utilisateur a choisi d'afficher la seconde partie du BWT étape par étape.
* 3eme onglet  :
    * Huffman qui va appeler les méthodes du fichier Huffman_G.py 
    * Importation du fichier format texte contenant la séquence ADN
    * Suite à cela l'application affichera la séquence charger et 3 options à réaliser dans l'ordre .Sauf pour l'abre de huffman, l'utilisateur peut choisir soit l'un ou l'autre ou les deux  :
        * Afficher la table de fréquence, c'est à dire le dictionnaire de fréquence d'apparition de chaque nucléotides dans la séquence .Exemple: {'N':7,'T':8,'A':3,'C':2,'G':5}  
        * Afficher l'arbre de Huffman direct c'est à dire le éesultat de la dernière incrémentation de l'arbre.
        * Afficher l'arbre de Huffman étape par étape.
    * Une fois l'arbre réalisé 7 options vont apparaitre:
        * Afficher le dictionnaire, 
        * Afficher la sequence binaire,
        * Afficher la sequence ADN décoder à partir de la sequence binaire,
        * Ecrire la sequence binaire sur un caractere c'est à dire en 8 bits via la table ASCII,
        * Convertir le caractere en binaire (donc retour au binaire), 
        * Convertir le binaire (réaliser ci-dessus) en séquence ADN donc retour à la sequence ADN de départ,
        * Sauvegarder la séquence ADN , binaire et caractère dans un fichier text.
        * Seul l'option du dictionnaire ne suit pas un ordre précis les autres doivent être réaliser dans l'ordre affiché. 
* 4eme onglet : BWT + Huffman
    * Input fichier ADN et affichage de la première etape de BWT (directement le résutat)
    * Suite à cela affichage de la partie deux de BWT (directement le résultat)
    * Enfin Huffman est alors réalisable 6 option appraissent à faire dans l'ordre 
    * Ici un choix a été pris:  ne pas afficher toute les options des algorithmes car elles ont déjà été afficher dans la partie onglet huffman (3eme onglet), et BWT onglet (2eme onglet)


## BWT.py 
Importation nécessaire au bon fonctionnement du code:

```python
from tkinter import *
import numpy as np
```

**REMARQUE :**
*  step by step -> cela signifie qu’on va afficher la matrice étape par étape donc on va afficher chaque itération . C'est la même chose que écrire étape par étape.
*  Ce fichier a été commenter en anglais, les noms de variables sont également en anglais.
*  Ce fichier a été fait en POO pour une utilisation plus simple de tkinter.


Deux variables global de classe:
Une pour mettre la sequence et l'autre c'est la longeur de la sequence +1
```python
        self.seq=seq
        self.len_seq=len(seq)+1
```


Le fichier se compose comme suit:
*  convert_list  : On va convertir la séquence en liste.Un nucléotide = un element de la liste et on ajoute un caractere spécial à la fin '$'.On convertit la séquence qui est en string en liste pour apres pouvoir l'implémenter dans une matrice numpy.
*  scrolling part : c'est toute la partie importante pour le défilement dans l'application tkinter. Il faut la mettre ici car elle est nécessaire pour le bon affichage du step by step.
*  **PARTIE 1**
*  fill_matrix : C'est la partie une de BWT .On va remplir la matrice numpy ligne par ligne.
*  lexicography_sort : Tri la matrice par ordre lexicograhique.
*  get_BWTseq : Récupére la derniere colonne de la matrice remplie c'est donc notre sequence BWT ( c'est à dire le séquence transformée).
*  **PARTIE 2**
*  Rebuild_matrix : Le but ici est de retrouver la sequence ADN de depart . Cela se fait colonne par colonne grâce âà la sequence BWT . A chaque itération on va trier lexicographiquement.
*  BWT_final : Fonction qui permet de retrouver le sequence ADN dans la matrice issue de la reconstruction .Puis affiche cette même sequence. 

## BWT_G.py

Il faut importer la librairie tkinter et le fichier BWT.py .
```python
from tkinter import *
from tkinter import ttk
```


**REMARQUE :**
*  Ce fichier a été fait en POO pour une utilisation plus simple de tkinter.

Le fichier se compose comme suit:
*  Input fichier : Via la fonction BWT_get_path_fichier on va stocker le chemin du fichier, ouvrir le fichier , le convertir en strint , le lire et enlever les caractères '\n'. 
*  **PARTIE 1**
* BWT etape 1 : 
    * BurrowsWeeler_step0 et BurrowsWeeler_step1 contiennent les fonctions a appeler du fichier BWT .
    * popup_step1 est une fonction qui fait l'affichage tkinter. C'est à dire le tkinter pour les pop up (fenètre flottante) quand l'utilisateur appuyera sur les options dans le mains.Alors cette fonction sera appeler et va générer un pop up.
*  **PARTIE 2**
* BWT etape 2 : 
    * BurrowsWeeler_step2 et BurrowsWeeler_final_step  contiennent les fonctions a appeler du fichier BWT.
    * popup_step2 même chose que popup_step1 mais pour la partie deux de BWT.

## HuffmanV3.py

Il faut importer la librairie tkinter, heapq .
```python
import heapq as hq
from tkinter import *
from tkinter import ttk 
```
**REMARQUE :**
*  Ce fichier a été fait en POO pour une utilisation plus simple de tkinter.


*  arbre_huffman : 
    * C'est une fonction qui va premettre la création et l'implémentation de l'arbre Huffman. On va d'abord crée une liste de liste . Chaque élement sera une liste contenant un nœud et son poid. Cela est réalisé grâce à la fonction make_abre_huffman.
    * Suite a cela on utilise heapify qui va permettre de transformer notre liste de liste en un format utilisable par la librairie heapq.
    * Tant que la liste de liste baptisée arbre dans le code à une longeur plus grande ou égale à deux, on va retirer les deux noeuds ayant le plus petit poids puis on va faire une liste de transition pour concatener les nucléotides correspondant et faire la somme des poids.Egalement,  implementer ceux deux mêmes noeuds dans une autre liste en rajoutant avec soit 1 soit 0
    * 
* make_dico : Fonction qui va prendre la liste retourner par la fonction arbre_huffman et crée un dictionnaire avec. Ainsi, on va avoir un dictionnaire de convertion avec pour chaque nucléotide son écriture binaire.
** -> Cette fonction sera utiliser pour le codage et le decodage de la sequence ADN**

## Huffman_G.py

Il faut importer la librairie tkinter et HuffmanV3  .
```python
from tkinter import *
from tkinter import ttk
from tkinter import filedialog

import HuffmanV3
```
**REMARQUE :**
*  Ce fichier a été fait en POO pour une utilisation plus simple de tkinter.

Ce fichier est organiser en plusieurs fonctions chacune d'elle fait appel à une fonction du fichier HuffmanV3.py et contient des labels tkinter pour l'affichage graphique.Le choix d'avoir mis cela dans une autre fonction et non dans le main directement est pour éviter d'avoir un main trop long (avec trop de ligne de code).

## Mot de la fin 
*  N'ont été détaillé que certaines fonctions.Ceux qui me semblaient soit importante soit dur à comprendre. 
*  Sauvegarde temporaire non réalisée par soucis de convertion nottament pour BWT avec le type que génère la librairie numpy.
*  On ne peut également pas donnée une séquence BWT en input cependant le programme detecte lorsqu'on met une séquence BWT (abordée dans l'onglet BWT uniquement ).
* **Pour les deux derniers points le code a été mis en commentaire dans le programme car cela à été abordée**








































































































































































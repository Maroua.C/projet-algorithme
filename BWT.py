#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  6 08:45:37 2020

@author: marwa     
"""

#############################################################################################
#___File which has the code of BWT and this file will be use for the graphique BWT part_____# 
#############################################################################################


#We have to import tkinter library because we ll use label for the step by step option
from tkinter import *
import numpy as np

class BurrowsWeeler:
    def __init__(self,seq):
        """ seq containt the DNA sequence and len_seq is the lenght of the DNA sequence """
        self.seq=seq
        self.len_seq=len(seq)+1
        
        
######################################
######______convert_list______########
######################################

    def convert_list(self,seq):
        """ put the seq into a list and add the '$' element which will be use
            to transform the sequence """
        
        #creat a list which will containt the seq  
        seq_list= []
        
        for  i in range(0,self.len_seq-1):
            #add nu by nu
            seq_list.append(seq[i])

        #add the '$' in the end of the list
        seq_list.append('$')
        
        print("LIST OF SEQ CREATED")
        print(seq_list)
        
        #return the variable because it will be use for other function
        return seq_list
    
    
######################################
######___create_empy_matrix___########
######################################

    def create_empy_matrix(self,i,j):
        """ Create a new empty numpy matrix  """            
        matrix_seq=np.full((i,j),fill_value='' )
        return matrix_seq
    
######################################
######______Scrolling part____########
######################################

    def create_scrolling_label(self,frame):
        """ Scrolling part"""
        	#Frame which have canvas for scrollin
        self.frame_scrolling =Frame(frame,bg='white')
        self.frame_scrolling.grid(column=2)
		
		#Canvas for the scrolling bar 
        self.cnv = Canvas(self.frame_scrolling,width=700)            
        self.cnv.grid(column=2)
		
        #Put the canvas into a label
        self.frm = Frame(self.cnv,bg='white',width=700)
        self.frm.grid(column=2)
		
		#Scrolling Horizontal
        vScroll = Scrollbar(self.frame_scrolling, orient=VERTICAL, command=self.cnv.yview)
        vScroll.grid(row=0,column=2, sticky='ns')

        self.cnv.configure(yscrollcommand=vScroll.set)
        
    def update_scrolling(self):
        """this function is use to keep the scrolling when we add someting of the frm frame and
            cnv canvas"""
        #scrolling
        self.frm.update()
        
        #create window in the canvas 
        self.cnv.create_window(1, 1, window=self.frm, anchor=NW)
        self.cnv.configure(scrollregion=self.cnv.bbox(ALL))


##################################################################################################################
#sequence DNA -> sequence BWT
 
		
######################################
######_______fill_matrix______########
######################################
    
    def fill_matrix(self,seq_list,matrix,yes,frame):
        """ Fill the numpy matrix by following the BWT rule this will be use 
            for compressing the seq """
        #oui use to know if we do the step by step part.
        if yes ==1:
           self.create_scrolling_label(frame)

        #Fill the matrix columns by columns
        matrix[0]=np.array([seq_list])
        
        for i in range(0,self.len_seq):
            #roll is use for the shift (by 1 on the right) 
            shift=np.roll(matrix[:1],i)
            matrix[i]=np.array([shift])
            
            #STEP BY STEP PART
            if yes == 1:
                L=Label(self.frm,text=matrix,fg="#075C69",font=("Georgia", 12))
                L.grid(column=3)                
                self.update_scrolling()
  
            #print(matrix)
        print("MATRIX IS FILLED ")    
        print(matrix)
        return matrix
    
######################################
######_____lexicography_sort__########
######################################
        
    def lexicography_sort(self,matrix_filled):
        """ the matrix will be sort. We will use some temporary list to do that """        
     
        #those two list will be helpful for the sort
        new_list=[]
        sorted_list=[]
        
        for i in range(0,self.len_seq):
            #select element and put them into a list 
            tmp=list(matrix_filled[i,:])
            new_list.append(tmp)
            #step by step
            #print(new_list)
        sorted_list= sorted(new_list) 
 
        print("LIST LEXICOGRAPHY SORT")
        print(sorted_list)  
        
        #verification that we have all the seq 
        #print(len(sorted_list))
        
        #convert list into matrix and return that matrix
        sorted_matrix=np.array(sorted_list)
        print("MATRIX LEXICOGRAPHY SORT")
        print(sorted_matrix)      
        return sorted_matrix


######################################
######_______list to str______########
######################################
        
    def list_to_str(self,list):
        """ convert list to str ,this will be usefull on the code"""
        str=""
        for i in range(len(list)):
            str = str + list[i]
        
        return str 

######################################
######_______get_BWTseq_______########
###################################### 
        
    def get_BWTseq(self,sorted_matrix):
        """return the last column of the lexicography matrix -> it's the BWT sequence"""
        BWTseq=self.lexicography_sort(sorted_matrix)
        BWTseq=self.list_to_str(BWTseq[0:self.len_seq,self.len_seq-1])        
        return BWTseq


##################################################################################################################
#sequence BWT -> sequence DNA
        

######################################
######_____Rebuild_matrix_____########
###################################### 
        
    def Rebuild_matrix(self,new_sorted_matrix,sorted_matrix,yes,frame):
        """ Second part of BWT, we have the tranformed seq and now we want the seq
            back """
        #yes is use to know if we do the step by step part (this will be use in the Tkinter ).
        if yes ==1:
           self.create_scrolling_label(frame)
  
        #fill the first columns 
        #we ll use a new matrix to rebuild the BWT and get the DNA sequence back 
        new_sorted_matrix[:,0]=sorted_matrix[0:self.len_seq,0]
        print(new_sorted_matrix)

        for i in range(1,self.len_seq):
            #add in the column of the new matrix the array we convert
            #matrix is sorted on each iteration

            new_sorted_matrix[:,i]=np.array(sorted_matrix[0:self.len_seq,i])
            #sort the matrix
            np.sort(new_sorted_matrix[1:self.len_seq,i]) 
            #shif 
            np.roll(new_sorted_matrix,1 ,axis=1) 

            #STEP BY STEP PART use for tkinter 
            if yes == 1:
                L=Label(self.frm,text=new_sorted_matrix,fg="#075C69",font=("Georgia", 12))
                L.grid(column=2)                
                self.update_scrolling()
            
            print(new_sorted_matrix)
  
        #add the last sequence after the loop because it won't sort if i do it inside
        new_sorted_matrix=np.roll(new_sorted_matrix,1 ,axis=1)
        new_sorted_matrix[:,0]=np.array(sorted_matrix[0:self.len_seq, self.len_seq-1])
        
        print("REBUILT SEQ MATRIX ")
        print(new_sorted_matrix)
        return new_sorted_matrix
    
        
    
    def BWT_final(self,matrix):
        """ Final process once we make the transformation we will extract the seq back 
            thanks to the '$' at the end of the final column 
            (we ll extract the line where the $ is) """
             
        #for a better visualisation
        a=self.len_seq
       
        #no need to do a loop because where will return the index and normally it should
        #return only 1 element because there are only one $ per collumns
        index=np.where(matrix[0:a,a-1:a] == '$')[0]
        print("FIND THE  $ IN THE LAST COLLUMS ")
        
        #put the line wich as the same index as index into a new matrix (so its transposed)
        back_seq=np.array(matrix[index])
        #print(back_seq)
        
        #convert into list (because it's easier to manipule than numpy and we only have one array)
        back_seq_list=np.array(back_seq)[0].tolist()
        #remove the $ from the list
        back_seq_list.remove('$')        
        #get the seq back !
        print("GET THE SEQ BACK ")
        print(back_seq_list)
        return back_seq_list
    

#seq="ACTTGATC"
#frame =""
#yes =0
#BurrowsWeeler=BurrowsWeeler(seq)
#seq_list=BurrowsWeeler.convert_list(seq)    
#matrix = BurrowsWeeler.create_empy_matrix(len(seq_list),len(seq_list))
#matrix_filled=BurrowsWeeler.fill_matrix(seq_list,matrix,frame,yes)
#
#sorted_matrix=BurrowsWeeler.lexicography_sort(matrix_filled)
#
#print(BurrowsWeeler.get_BWTseq(sorted_matrix))
#
#new_sorted_matrix=BurrowsWeeler.create_empy_matrix(9,9)
#new_sorted_matrix.transpose()
#rebuil_matrix=BurrowsWeeler.Rebuild_matrix(new_sorted_matrix,sorted_matrix,0,frame)
#
#BurrowsWeeler.BWT_final(rebuil_matrix)
